package com.practice;

public class GuessGame {
    Player p1;
    Player p2;
    Player p3;

    public void startGame(){
        p1 = new Player();
        p2 = new Player();
        p3 = new Player();

        int guessp1 = 0;
        int guessp2 = 0;
        int guessp3 = 0;

        boolean p1IsRight = false;
        boolean p2IsRight = false;
        boolean p3IsRight = false;

        int target = (int)(Math.random()*10);

        while(true){
            System.out.println("Number to guess is: "+target);
            p1.guess();
            p2.guess();
            p3.guess();
            guessp1 = p1.number;
            guessp2 = p2.number;
            guessp3 = p3.number;

            if(guessp1 == target)
                p1IsRight = true;
            if(guessp2 == target)
                p2IsRight = true;
            if(guessp3 == target)
                p3IsRight = true;

            if(p1IsRight || p2IsRight || p3IsRight){
                System.out.println("We have the winner!");
                System.out.println("Player1 got the right: "+ p1IsRight);
                System.out.println("Player2 got the right: "+ p2IsRight);
                System.out.println("Player3 got the right: "+ p3IsRight);
                System.out.println("Game Over!!!");
                break;
            }
            else
                System.out.println("Didn't match. Try again...");
        }

    }
}
